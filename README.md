# Weather-App

This is an Android application written in Kotlin using the MVVM architecture pattern. The application's main purpose is to display weather information for the user's current location and other locations that the user choose.



![Screenshot](https://live.staticflickr.com/65535/52806377523_e7c411bd56_h.jpg)
