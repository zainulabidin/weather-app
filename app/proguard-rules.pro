# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\Program Files\Android\Android Studio\gradle\gradle-6.7.1\bin\..\gradle\wrapper\gradle-wrapper.properties
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.

# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Retrofit 2.X
-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn javax.annotation.**
-keep class com.squareup.okhttp3.** { *; }
-keep class com.squareup.okio.** { *; }
-keep class javax.annotation.** { *; }
-keep class retrofit2.** { *; }
-keepattributes Signature

-keepattributes *Annotation*
-keepclassmembers class ** {
    public void onEvent*(**);
}

# To maintain generic signatures of APIs
-keepattributes Signature

# Keep classes that are referenced in the AndroidManifest.xml file
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
