package com.task.weatherforcast.data.managers

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Looper
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices

class LocationManager(private val context: Context) {

    private var fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
    private var locationRequest =
        LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(600000).setFastestInterval(1 * 1000)
    private var locationCallback: LocationCallback? = null
    private var locationListener: ((Location?) -> Unit)? = null
    private var location: Location? = null

    fun getLocation(callback: (Location?) -> Unit) {
        locationListener = callback
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            getLocationCallback(),
            Looper.myLooper()!!
        )
    }

    private fun getLocationCallback(): LocationCallback {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(lr: LocationResult) {
                location = if (lr.locations.isNotEmpty()) {
                    lr.locations[0]
                } else {
                    lr.lastLocation
                }
                locationListener?.invoke(location)

                stopLocationUpdates()
            }
        }

        return locationCallback as LocationCallback
    }

    fun stopLocationUpdates() {
        locationCallback?.let {
            fusedLocationClient.removeLocationUpdates(it)
            locationCallback = null
        }
    }

    interface LocationDataSource {
        fun getCurrentLocation(callback: (Location?) -> Unit)
    }


}
