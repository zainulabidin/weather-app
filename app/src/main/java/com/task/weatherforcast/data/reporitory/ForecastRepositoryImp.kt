package com.task.weatherforcast.data.reporitory

import android.location.Location
import com.task.weatherforcast.data.managers.LocationManager
import com.task.weatherforcast.data.retrofit.Endpoints
import com.task.weatherforcast.data.retrofit.RequestStates
import com.task.weatherforcast.domain.interfaces.ForecastRepository
import com.task.weatherforcast.domain.model.ForecastResponse
import com.task.weatherforcast.utils.validateError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

// To full fill the cache requirement we can further split the Repository into Data (remote) and local source.

class ForecastRepositoryImp @Inject constructor(
    private val endpoints: Endpoints,
    private val locationDataSource: LocationManager.LocationDataSource
): ForecastRepository {


    override fun getCurrentLocation(callback: (Location?) -> Unit) {
        locationDataSource.getCurrentLocation(callback)
    }

    override fun getForecastByLocation(
        location: Location?,
        appId: String,
    ): Flow<RequestStates<ForecastResponse>> {
        return flow {
            emit(RequestStates.Loading)
            val requestResponse = endpoints.getForecast(
                location?.latitude.toString(), location?.longitude.toString(), appId, "metric"
            )
            if (requestResponse.code == "200") emit(RequestStates.Success(requestResponse))
            else emit(RequestStates.Error(java.lang.Exception(requestResponse.code)))
        }.catch {
            validateError(this, it)
        }.flowOn(Dispatchers.IO)
    }

    override fun getForecastByCity(
        city: String,
        appId: String,
    ): Flow<RequestStates<ForecastResponse>> {
        return flow {
            emit(RequestStates.Loading)
            val requestResponse = endpoints.getForecastByCity(city, appId, "metric")
            if (requestResponse.code == "200") emit(RequestStates.Success(requestResponse))
            else emit(RequestStates.Error(java.lang.Exception(requestResponse.code)))
        }.catch {
            validateError(this, it)
        }.flowOn(Dispatchers.IO)
    }

}