package com.task.weatherforcast.data.reporitory

import android.location.Location
import com.task.weatherforcast.data.managers.LocationManager
import javax.inject.Inject
import javax.inject.Singleton

@Singleton class LocationDataSourceImpl @Inject constructor(private val locationManager: LocationManager) :
    LocationManager.LocationDataSource {
    override fun getCurrentLocation(callback: (Location?) -> Unit) {
        locationManager.getLocation { location ->
            callback(location)
        }
    }
}

