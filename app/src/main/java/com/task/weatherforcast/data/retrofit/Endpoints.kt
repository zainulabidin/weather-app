package com.task.weatherforcast.data.retrofit


import com.task.weatherforcast.domain.model.ForecastResponse
import retrofit2.http.*

interface Endpoints {

    @GET("forecast")
    suspend fun getForecast(
        @Query("lat") lat:String,
        @Query("lon") lng:String,
        @Query("appid") appId:String,
        @Query("units") units:String,
    ): ForecastResponse
    @GET("forecast")
    suspend fun getForecastByCity(
        @Query("q") city:String,
        @Query("appid") appId:String,
        @Query("units") units:String,
    ): ForecastResponse


}