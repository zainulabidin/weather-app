package com.task.weatherforcast.domain.interfaces

import android.location.Location
import com.task.weatherforcast.data.retrofit.RequestStates
import com.task.weatherforcast.domain.model.ForecastResponse
import kotlinx.coroutines.flow.Flow

interface ForecastRepository {
    fun getCurrentLocation(callback: (Location?) -> Unit)
    fun getForecastByLocation(
        location: Location?,
        appId: String,
    ): Flow<RequestStates<ForecastResponse>>
    fun getForecastByCity(
        city: String,
        appId: String,
    ): Flow<RequestStates<ForecastResponse>>
}
