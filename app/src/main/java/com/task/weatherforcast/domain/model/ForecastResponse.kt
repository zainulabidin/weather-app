package com.task.weatherforcast.domain.model

import com.google.gson.annotations.SerializedName

data class ForecastResponse(
    @SerializedName("cod")
    val code: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("list")
    val forecastListItems: ArrayList<ForecastListItem>,
    @SerializedName("city")
    val forecastCity: ForecastCity

) {
    data class ForecastListItem(
        @SerializedName("dt_txt")
        val dateTime: String,
        @SerializedName("main")
        val forecastListItemMain: ForecastListItemMain,
        @SerializedName("weather")
        val forecastListItemWeathers: ArrayList<ForecastListItemWeather>,
        @SerializedName("wind")
        val forecastListItemWind: ForecastListItemWind,
        @SerializedName("visibility")
        val visibility: Double,
    ) {
        data class ForecastListItemMain(
            @SerializedName("temp")
            val temp: Float,
            @SerializedName("feels_like")
            val feelsLike: Float,
            @SerializedName("temp_min")
            val tempMin: Float,
            @SerializedName("temp_max")
            val tempMax: Float,
            @SerializedName("pressure")
            val pressure: Float,
            @SerializedName("sea_level")
            val seaLevel: Float,
            @SerializedName("grnd_level")
            val groundLevel: Float,
            @SerializedName("humidity")
            val humidity: Float,
            @SerializedName("temp_kf")
            val tempKf: Float,
        )

        data class ForecastListItemWeather(
            @SerializedName("main")
            val title: String,
            @SerializedName("description")
            val description: String,
            @SerializedName("icon")
            val icon: String,
        )

        data class ForecastListItemWind(
            @SerializedName("speed")
            val speed: Float,
            @SerializedName("deg")
            val deg: Float,
            @SerializedName("gust")
            val gust: Float,
        )
    }

    data class ForecastCity(
        @SerializedName("name")
        val name: String
    )
}