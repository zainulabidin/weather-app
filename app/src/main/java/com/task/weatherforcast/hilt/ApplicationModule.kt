package com.task.weatherforcast.hilt

import android.content.Context
import androidx.databinding.ktx.BuildConfig
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.task.weatherforcast.R
import com.task.weatherforcast.data.managers.LocationManager
import com.task.weatherforcast.data.reporitory.ForecastRepositoryImp
import com.task.weatherforcast.data.reporitory.LocationDataSourceImpl
import com.task.weatherforcast.data.retrofit.Endpoints
import com.task.weatherforcast.domain.interfaces.ForecastRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {

    @Provides
    @Singleton
    fun provideOkHttpClient() = if (BuildConfig.DEBUG) {

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    } else OkHttpClient
        .Builder()
        .build()


    @Singleton
    @Provides
    fun provideRetrofit(@ApplicationContext context: Context, gson: Gson, okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(context.getString(R.string.base_url_weather_api))
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(okHttpClient)
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun provideApiService(retrofit: Retrofit): Endpoints = retrofit.create(Endpoints::class.java)

    @Singleton
    @Provides
    fun provideRepository(
        endpoints: Endpoints,
        locationDataSource: LocationManager.LocationDataSource
    ) = ForecastRepositoryImp(endpoints, locationDataSource)

    @Provides
    fun provideLocationManager(@ApplicationContext context: Context): LocationManager {
        return LocationManager(context)
    }

    @Provides
    @Singleton
    fun provideLocationDataSource(locationManager: LocationManager): LocationManager.LocationDataSource {
        return LocationDataSourceImpl(locationManager)
    }

    @Provides
    fun provideForecastRepository(
        endpoints: Endpoints,
        locationDataSource: LocationManager.LocationDataSource
    ): ForecastRepository {
        return ForecastRepositoryImp(endpoints, locationDataSource)
    }
}