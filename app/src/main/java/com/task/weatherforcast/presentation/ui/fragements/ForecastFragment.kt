package com.task.weatherforcast.presentation.ui.fragements

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.task.weatherforcast.R
import com.task.weatherforcast.data.retrofit.RequestStates
import com.task.weatherforcast.databinding.FragmentHomeBinding
import com.task.weatherforcast.databinding.LayoutForCitiesDialogBinding
import com.task.weatherforcast.domain.model.ForecastResponse
import com.task.weatherforcast.presentation.ui.fragements.adapter.AdapterForTenDaysForecastRv
import com.task.weatherforcast.presentation.viewmodels.ForecastViewModel
import com.task.weatherforcast.utils.Constants
import com.task.weatherforcast.utils.ToastUtils
import com.task.weatherforcast.utils.hasLocationPermission
import com.task.weatherforcast.utils.isGPSEnabled
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Locale
import kotlin.math.roundToInt


@AndroidEntryPoint class ForecastFragment : Fragment() {

    private val permissions = arrayOf(ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION)
    private lateinit var viewModel: ForecastViewModel
    private lateinit var dataBinding: FragmentHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        viewModel = ViewModelProvider(this)[ForecastViewModel::class.java]

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requestLocationPermissions()
        registerViewModelObserver()
        initObjects()
        initClickListeners(view)


    }

    private fun initObjects() {
        // forecast adapter
        if (dataBinding.rvWeeklyForecast.adapter == null) dataBinding.rvWeeklyForecast.adapter =
            AdapterForTenDaysForecastRv(arrayListOf())
    }

    private fun initClickListeners(view: View) {
        dataBinding.tvChangeCity.setOnClickListener { changeCityClickListener(view) }

    }

    private fun requestLocationPermissions() {
        if (requireContext().hasLocationPermission()) {
            if (isGPSEnabled(requireContext())) requestCurrentUserLocation() else turnOnGPS()
        } else multiplePermissionsLauncher.launch(permissions)
    }

    private val multiplePermissionsLauncher = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { result ->
        if (result[ACCESS_FINE_LOCATION] == true || result[ACCESS_COARSE_LOCATION] == true) {
            if (isGPSEnabled(requireContext())) requestCurrentUserLocation() else turnOnGPS()
        } else if (result[ACCESS_FINE_LOCATION] == false || result[ACCESS_COARSE_LOCATION] == false) {
            ToastUtils.showCustomToast(
                requireContext(), getString(R.string.location_permission_msq)
            )
        }
    }

    private fun requestCurrentUserLocation() {
        viewModel.fetchCurrentLocation()
    }

    private val locationSettingsRequestLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (isGPSEnabled(requireContext())) {
                requestCurrentUserLocation()
            }
        }

    private fun turnOnGPS() {
        val intent = Intent(ACTION_LOCATION_SOURCE_SETTINGS)
        locationSettingsRequestLauncher.launch(intent)
    }

    private fun requestForecastByLocation(location: Location?) {
        viewModel.getForecastByLocation(location, getString(R.string.app_id_weather_api))
    }

    private fun registerViewModelObserver() {
        // observer location and forecast state from viewModel and update the view
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.CREATED) {
                launch {
                    viewModel.forecastListObserver.collectLatest { requestStatuses ->
                        handleForecastRequestStatus(
                            requestStatuses
                        )
                    }
                }
                launch {
                    viewModel.location.filterNotNull()
                        .collectLatest { requestForecastByLocation(it) }
                }
            }
        }
    }

    private fun handleForecastRequestStatus(requestStatuses: RequestStates<ForecastResponse>) {
        when (requestStatuses) {
            is RequestStates.Loading -> {
                handleUIForLoadingForecast()
            }
            is RequestStates.Success -> {
                handleUIForSuccessForecast(requestStatuses)
            }
            is RequestStates.NoInternet -> {
                handleUIForNoInternetForecast()
            }
            is RequestStates.Error -> {
                handleUIForErrorForecast(requestStatuses)
            }
        }
    }

    private fun handleUIForLoadingForecast() {
        dataBinding.groupLoadingData.visibility = View.VISIBLE
        dataBinding.tvTemp.visibility = View.INVISIBLE
    }

    private fun handleUIForSuccessForecast(requestStatuses: RequestStates.Success<ForecastResponse>) {
        with(dataBinding) {
            groupLoadingData.visibility = View.INVISIBLE
            tvTemp.visibility = View.VISIBLE

            val todayForecast = requestStatuses.data.forecastListItems.first()
            tvCityName.text = requestStatuses.data.forecastCity.name
            tvTemp.text = context?.getString(
                R.string.forecast_temp,
                todayForecast.forecastListItemMain.temp.roundToInt().toString()
            )
            tvHumidityValue.text =
                String.format("%d%%", todayForecast.forecastListItemMain.humidity.roundToInt())
            tvWindValue.text = context?.getString(
                R.string.forecast_wind_speed,
                todayForecast.forecastListItemWind.speed.roundToInt().toString()
            )
            tvMode.text = todayForecast.forecastListItemWeathers.first().title
            tvVisibilityValue.text = context?.getString(
                R.string.forecast_visibility_km, (todayForecast.visibility / 1000.0)
            )

            val dateFormatter = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault())
            tvDate.text = dateFormatter.parse(todayForecast.dateTime)?.let { date ->
                SimpleDateFormat("EEEE,dd MMMM", Locale.getDefault()).format(date)
            }

            tvDailySummaryValue.text = getDailySummary(todayForecast)

            val dailyForecast = requestStatuses.data.forecastListItems.distinctBy { item ->
                dateFormatter.parse(item.dateTime)?.let {
                    SimpleDateFormat("dd MMM", Locale.getDefault()).format(it)
                }
            }.toMutableList()
            dailyForecast.removeAt(0)
            // set adapter for 5 days weather forecast
            rvWeeklyForecast.adapter = AdapterForTenDaysForecastRv(dailyForecast)
        }
    }

    private fun getDailySummary(todayForecast: ForecastResponse.ForecastListItem): String {
        val dailySummaryBuilder = StringBuilder()
        dailySummaryBuilder.append(
            getString(
                R.string.forecast_now_temp,
                dataBinding.tvTemp.text,
                todayForecast.forecastListItemMain.feelsLike.toString()
            )
        )
        dailySummaryBuilder.append("\n")
        dailySummaryBuilder.append(
            getString(
                R.string.forecast_today_temp,
                todayForecast.forecastListItemMain.tempMin.roundToInt().toString(),
                todayForecast.forecastListItemMain.tempMax.roundToInt().toString()
            )
        )
        return dailySummaryBuilder.toString()
    }


    private fun handleUIForNoInternetForecast() {
        dataBinding.groupLoadingData.visibility = View.INVISIBLE
        dataBinding.tvTemp.visibility = View.VISIBLE
        ToastUtils.showCustomToast(requireContext(), "No Internet")
    }

    private fun handleUIForErrorForecast(requestStatuses: RequestStates.Error) {
        dataBinding.groupLoadingData.visibility = View.INVISIBLE
        dataBinding.tvTemp.visibility = View.VISIBLE
        ToastUtils.showCustomToast(requireContext(), requestStatuses.exception.message ?: "")
    }

    private fun showCitiesDialog(
        context: Context,
        onItemSelected: (String) -> Unit,
    ): Dialog {
        val dialog = Dialog(context)
        val binding = DataBindingUtil.inflate<LayoutForCitiesDialogBinding>(
            LayoutInflater.from(context), R.layout.layout_for_cities_dialog, null, false
        )
        dialog.apply {
            setContentView(binding.root)
            window?.setBackgroundDrawableResource(R.color.colorDialogBg)
            window?.setLayout(
                ConstraintLayout.LayoutParams.MATCH_PARENT,
                ConstraintLayout.LayoutParams.MATCH_PARENT
            )
            setCancelable(true)
            show()
        }

        binding.run {
            val dialogTextViewClick = View.OnClickListener {
                onItemSelected.invoke((it as TextView).text.toString())
                dialog.dismiss()
            }
            arrayOf(tvCurrentLocation, tvKrakow, tvGdaSk, tvWarsaw, tvWrocAw, tvPozna).forEach {
                it.setOnClickListener(dialogTextViewClick)
            }
        }

        return dialog
    }

    override fun onDetach() {
        super.onDetach()
        locationSettingsRequestLauncher.unregister()
        multiplePermissionsLauncher.unregister()
    }


    private val changeCityClickListener: (View) -> Unit = {
        showCitiesDialog(requireContext()) { city ->
            if (city == Constants.CURRENT_LOCATION) requestLocationPermissions()
            else viewModel.getForecastByCity(city, getString(R.string.app_id_weather_api))
        }
    }
}