package com.task.weatherforcast.presentation.ui.fragements.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.task.weatherforcast.R
import com.task.weatherforcast.databinding.LayoutForTenDaysForecastBinding
import com.task.weatherforcast.domain.model.ForecastResponse
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

class AdapterForTenDaysForecastRv(
    val arrayList: MutableList<ForecastResponse.ForecastListItem>
) : RecyclerView.Adapter<AdapterForTenDaysForecastRv.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: LayoutForTenDaysForecastBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.layout_for_ten_days_forecast,
                parent,
                false
            )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (arrayList.size < 10) arrayList.size else 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(position)
    }

    inner class ViewHolder(private val binding: LayoutForTenDaysForecastBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(position: Int) {
            val context = binding.root.context
            val currentObj = arrayList[position]
            val dateFormatter = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault())
            binding.tvDate.text =
                dateFormatter.parse(currentObj.dateTime)?.let { date ->
                    SimpleDateFormat("dd MMM", Locale.getDefault()).format(
                        date
                    )
                }
            binding.tvTemp.text = context.getString(R.string.forecast_temp, currentObj.forecastListItemMain.temp.roundToInt().toString())

            when (currentObj.forecastListItemWeathers.first().icon) {
                "01d", "01n" -> binding.ivIcon.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.w01d
                    )
                )
                "02d", "02n" -> binding.ivIcon.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.w02d
                    )
                )
                "04d", "04n", "03n" -> binding.ivIcon.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.w04d
                    )
                )
                "09d", "09n" -> binding.ivIcon.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.w09d
                    )
                )
                "10d", "10n" -> binding.ivIcon.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.w10d
                    )
                )
                "11d", "11n" -> binding.ivIcon.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.w11d
                    )
                )
                "13d", "13n" -> binding.ivIcon.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.w13d
                    )
                )
            }
        }
    }
}