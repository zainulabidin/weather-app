package com.task.weatherforcast.presentation.viewmodels

import android.location.Location
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.task.weatherforcast.data.retrofit.RequestStates
import com.task.weatherforcast.domain.interfaces.ForecastRepository
import com.task.weatherforcast.domain.model.ForecastResponse
import com.task.weatherforcast.utils.NetworkHelper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel class ForecastViewModel @Inject constructor(
    private val forecastRepository: ForecastRepository, private val networkHelper: NetworkHelper
) : ViewModel() {

    private val _location = MutableStateFlow<Location?>(null)
    val location: StateFlow<Location?>
        get() = _location
    private val _forecastListResponse =
        MutableStateFlow<RequestStates<ForecastResponse>>(RequestStates.Loading)
    val forecastListObserver: StateFlow<RequestStates<ForecastResponse>>
        get() = _forecastListResponse

    fun fetchCurrentLocation() {
        forecastRepository.getCurrentLocation { location ->
            _location.value = location
        }
    }

    fun getForecastByLocation(location: Location?, appId: String) = viewModelScope.launch {
        if (networkHelper.isNetworkConnected()) {
            forecastRepository.getForecastByLocation(location, appId).onEach {
                _forecastListResponse.value = it
            }.catch { _forecastListResponse.value = RequestStates.Error(Exception(it.message)) }
                .collect()
        } else {
            _forecastListResponse.value = RequestStates.NoInternet
        }
    }

    fun getForecastByCity(
        city: String,
        appId: String,
    ) = viewModelScope.launch {
        if (networkHelper.isNetworkConnected()) {
            forecastRepository.getForecastByCity(city, appId).onEach {
                _forecastListResponse.value = it
            }.catch { _forecastListResponse.value = RequestStates.Error(Exception(it.message)) }
                .collect()
        } else {
            _forecastListResponse.value = RequestStates.NoInternet
        }
    }
}
