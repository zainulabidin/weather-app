package com.task.weatherforcast.utils

import android.content.Context
import android.widget.Toast


object ToastUtils {

    fun showCustomToast(context: Context, message: String) {
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show()
    }
}