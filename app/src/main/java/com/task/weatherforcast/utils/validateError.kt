package com.task.weatherforcast.utils

import com.task.weatherforcast.data.retrofit.RequestStates
import kotlinx.coroutines.flow.FlowCollector
import org.json.JSONObject
import retrofit2.HttpException

 suspend fun <T> validateError(
    flowCollector: FlowCollector<RequestStates<T>>,
    e: Throwable,
) {
    when {
        e is HttpException -> {
            when {
                e.code() == 500 && e.code() == 404 -> {
                    flowCollector.emit(RequestStates.Error(e))
                }
                e.code() == 400 -> {
                    val errorJson = e.response()?.errorBody()?.source()?.buffer?.readUtf8()!!
                    val jsonObject = JSONObject(errorJson)
                    jsonObject.get("message")
                    flowCollector.emit(RequestStates.Error(Exception(jsonObject.getString("message"))))
                }
                else -> {
                    flowCollector.emit(RequestStates.Error(e))

                }
            }
        }
        e.message != null && e.message!!.contains(
            "SSL handshake timed out", true
        ) || e.toString().contains("UnknownHostException", true) || e.toString()
            .contains("SocketTimeoutException", true) -> {
            flowCollector.emit(RequestStates.NoInternet)
        }
        else -> flowCollector.emit(RequestStates.Error(java.lang.Exception(e)))
    }
}