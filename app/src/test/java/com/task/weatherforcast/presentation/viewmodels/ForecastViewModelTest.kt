package com.task.weatherforcast.presentation.viewmodels

import android.location.Location
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verifyZeroInteractions
import com.nhaarman.mockitokotlin2.whenever
import com.task.weatherforcast.data.reporitory.ForecastRepositoryImp
import com.task.weatherforcast.data.retrofit.RequestStates
import com.task.weatherforcast.domain.model.ForecastResponse
import com.task.weatherforcast.utils.NetworkHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`


@ExperimentalCoroutinesApi class ForecastViewModelTest {

    private lateinit var viewModel: ForecastViewModel
    private val forecastRepository: ForecastRepositoryImp = mock()
    private val networkHelper: NetworkHelper = mock()
    private val location: Location = mock()
    private val city = "Krakow"
    private val appId = "1234567890"

    @Before fun setUp() {
        Dispatchers.setMain(Dispatchers.Unconfined)
        viewModel = ForecastViewModel(forecastRepository, networkHelper)
    }

    @Test fun `test getForecast with successful response`() = runTest(UnconfinedTestDispatcher()) {
        val forecastResponse = ForecastResponse(
            "code", "message", arrayListOf(
                ForecastResponse.ForecastListItem(
                    "dateTime", ForecastResponse.ForecastListItem.ForecastListItemMain(
                        1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f
                    ), arrayListOf(
                        ForecastResponse.ForecastListItem.ForecastListItemWeather(
                            "title", "description", "icon"
                        )
                    ), ForecastResponse.ForecastListItem.ForecastListItemWind(
                        10.0f, 11.0f, 12.0f
                    ), 13.0
                )
            ), ForecastResponse.ForecastCity(city)
        )
        val responseFlow =
            MutableStateFlow<RequestStates<ForecastResponse>>(RequestStates.Success(forecastResponse))
        `when`(forecastRepository.getForecastByLocation(location, appId)).thenReturn(responseFlow)
        `when`(networkHelper.isNetworkConnected()).thenReturn(true)

        viewModel.getForecastByLocation(location, appId)

        viewModel.forecastListObserver.first {
            it is RequestStates.Success
        }
    }


    @Test fun `getForecast should return no internet response when network is not connected`() =
        runTest(UnconfinedTestDispatcher()) {
            whenever(networkHelper.isNetworkConnected()).thenReturn(false)

            viewModel.getForecastByLocation(location, appId)

            viewModel.forecastListObserver.first {
                it is RequestStates.NoInternet
            }
            verifyZeroInteractions(forecastRepository)
        }

    @Test fun `getForecast should return error response when an exception occurs`() =
        runTest(UnconfinedTestDispatcher()) {
            val exception = Exception("Something went wrong")
            val responseFlow = flow<RequestStates<ForecastResponse>> { throw exception }
            `when`(forecastRepository.getForecastByLocation(location, appId)).thenReturn(responseFlow)
            `when`(networkHelper.isNetworkConnected()).thenReturn(true)

            viewModel.getForecastByLocation(location, appId)

            viewModel.forecastListObserver.first {
                it is RequestStates.Error
            }
        }

    @Test fun `getForecast should emit Loading state when called`() =
        runTest(UnconfinedTestDispatcher()) {
            val responseFlow =
                MutableStateFlow<RequestStates<ForecastResponse>>(RequestStates.Loading)
            `when`(forecastRepository.getForecastByLocation(location, appId)).thenReturn(responseFlow)
            `when`(networkHelper.isNetworkConnected()).thenReturn(true)

            viewModel.getForecastByLocation(location, appId)

            // Await for the first state emitted by the observer
            viewModel.forecastListObserver.first {
                it is RequestStates.Loading
            }
        }


}


